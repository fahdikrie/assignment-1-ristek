import React from 'react';
import ReactDOM from 'react-dom';
import MediaQuery from 'react-responsive'

import './index.css';
import * as serviceWorker from './serviceWorker';


import MobileNavigationBar from './components/MobileNavigationBar/MobileNavigationBar.js';
import NavigationBar from './components/NavigationBar/NavigationBar.js';
import EditProfileCard from './components/EditProfileCard/EditProfileCard.js';
import Background from './components/Background/Background.js'

ReactDOM.render(
  <React.StrictMode>
    <Background/>

    <MediaQuery minDeviceWidth={950}>
      <NavigationBar/>
    </MediaQuery>
    <MediaQuery maxDeviceWidth={950}>
      <MobileNavigationBar />
    </MediaQuery>

    <EditProfileCard/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
