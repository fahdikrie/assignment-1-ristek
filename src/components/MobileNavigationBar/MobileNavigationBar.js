import React, { Component, Fragment } from 'react';

import { Navbar, Nav } from 'react-bootstrap';
import NavBrandCollapsible from './components/navbrandcollapsible';
import NavItemsCollapsible from './components/navitemscollapsible';
import NavProfileCollapsible from './components/navprofilecollapsible';
import 'bootstrap/dist/css/bootstrap.min.css';
import './MobileNavigationBar.css';


class MobileNavigationBar extends Component {

  render() {
    return(
      <div className="MobileNavigationBar">
        <Navbar bg="light" expand="lg">
          <Navbar.Brand href="#">
            <NavBrandCollapsible/>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="navItemsCollapsibleWrapper mr-auto">
              <NavProfileCollapsible/>
              <NavItemsCollapsible/>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
    )
  }

}

export default MobileNavigationBar;