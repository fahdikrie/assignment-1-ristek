import React, { Component, Fragment } from 'react';
import './navbrandcollapsible.css'

import logoPmb from "../../../assets/pmb-logo.png";
import logoRistek from "../../../assets/ristek-logo.png";


function NavBrandCollapsible(props) {
	return (
    <div className="navBrand">
      <img
        className="logo ristekLogo"
        alt="ristek"
        src={logoRistek}>
      </img>
      <img
        className="logo pmbLogo"
        alt="pmb"
        src={logoPmb}>
      </img>
    </div>
	);
}

export default NavBrandCollapsible;
