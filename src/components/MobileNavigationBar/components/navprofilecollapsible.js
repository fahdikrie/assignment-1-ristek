import React, { Component, Fragment } from 'react';
import './navprofilecollapsible.css';

import iconProfilePhoto from "../../../assets/profile.png";

function NavProfileCollapsible(props) {
  return (
    <div className="navProfileCollapsible">
      <img
        className="iconCollapsible profilePhotoCollapsible"
        alt="pp"
        src={iconProfilePhoto}>
      </img>
    </div>
    );
}

export default NavProfileCollapsible;