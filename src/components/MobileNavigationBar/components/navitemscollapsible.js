import React, { Component, Fragment } from 'react';
import './navitemscollapsible.css';

function navItemsCollapsible() {
	return (
    <div className="navItemsCollapsible">
      <a className="listItemsCollapsible" href="/">Task</a>
      <a className="listItemsCollapsible" href="/">Event</a>
      <a className="listItemsCollapsible" href="/">Announcement</a>
      <a className="listItemsCollapsible" href="/">Friends</a>
      <a className="listItemsCollapsible" href="/">Story</a>
      <a className="listItemsCollapsible" href="/">More</a>
    </div>
	);
}

export default navItemsCollapsible;