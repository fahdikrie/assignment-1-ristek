import React, { Component, Fragment } from 'react';
import './Background.css'
import BackgroundImg from "../../assets/desktop-footer-fade.png";

function Background(props) {
	return (
    <img
      className="background-footer"
      alt="background-footer"
      src={BackgroundImg}>
    </img>
	);
}

export default Background;
