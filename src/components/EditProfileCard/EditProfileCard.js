import React, { Component, Fragment } from 'react';

import Card from './components/card';
import './EditProfileCard.css'

class EditProfileCard extends Component {

  render() {
    return(
      <div className="EditProfileCard">
        <h3>Edit Profile</h3>
        <Card/>
      </div>
    )
  }

}

export default EditProfileCard;