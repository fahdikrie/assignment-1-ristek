import React, { Component, Fragment } from 'react';
import './editprofileform.css'

function EditProfileForm(props) {
	return (
    <div className="EditProfileForm">
      
      <div className="form-input form-input-title">
        <label>Title</label>
        <input type="text"></input>
      </div>

      <div className="form-group">
        <div className="form-input">
          <label>Birth Place</label>
          <input type="text"></input>
        </div>
        <div className="form-input">
          <label>Email</label>
          <input type="text"></input>
        </div>
      </div>

      <div className="form-group">
        <div className="form-input">
          <label>Birth Date</label>
          <input type="text"></input>
        </div>
        <div className="form-input">
          <label>LINE ID</label>
          <input type="text"></input>
        </div>
      </div>

      <div className="form-group">
        <div className="form-input">
          <label>High School</label>
          <input type="text"></input>
        </div>
        <div className="form-input">
          <label>Website</label>
          <input type="text"></input>
        </div>
      </div>
    </div>
	);
}

export default EditProfileForm;
