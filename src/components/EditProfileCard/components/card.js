import React, { Component, Fragment } from 'react';
import './card.css'

import Button from './button';
import EditProfileForm from './editprofileform';
import iconProfilePhoto from "../../../assets/profile.png";

function Card(props) {
	return (
    <div className="card">
      <div className="edit-card-header">
        <div className="header-pp">
          <img
            className="icon profilePhoto lg"
            alt="pp"
            src={iconProfilePhoto}>
          </img>
        </div>
        <div className="header-info">
          <h2>Muhammad Fathurziki Herlando</h2>
          <h5>Ilmu Komputer 2018</h5>
        </div>
      </div>
      <Button
          btnStyle="green"
          btnMsg={<p>Change Photo Profile</p>}
        />
      <Button
          btnStyle="red"
          btnMsg={<p>Delete Photo Profile</p>}
        />
      <EditProfileForm />
      <Button
          btnStyle="green save-btn"
          btnMsg={<p>Save</p>}
        />
    </div>
	);
}

export default Card;
