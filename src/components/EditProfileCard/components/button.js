import React, { Component, Fragment } from 'react';
import './button.css'

function Button(props) {
	return (
    <button className={'button ' + (props.btnStyle || '')}>
      {props.btnMsg}
    </button>
	);
}

export default Button;
