import React, { Component, Fragment } from 'react';

import NavBrand from './components/navbrand';
import NavItems from './components/navitems';
import NavProfile from './components/navprofile';
import './NavigationBar.css';

class NavigationBar extends Component {

  render() {
    return(
      <div className="NavigationBar">
        <NavBrand/>
        <NavItems />
        <NavProfile />
      </div>
    )
  }

}

export default NavigationBar;