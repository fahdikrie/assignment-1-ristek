import React, { Component, Fragment } from 'react';
import './navitems.css';

function NavItems() {
	return (
    <div className="navItems">
      <a className="listItems" href="/">Task</a>
      <a className="listItems" href="/">Event</a>
      <a className="listItems" href="/">Announcement</a>
      <a className="listItems" href="/">Friends</a>
      <a className="listItems" href="/">Story</a>
      <a className="listItems" href="/">More</a>
    </div>
	);
}

export default NavItems;