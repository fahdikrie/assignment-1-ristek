import React, { Component, Fragment } from 'react';
import './navprofile.css';

import iconBell from "../../../assets/bell.png";
import iconProfilePhoto from "../../../assets/profile.png";

function NavProfile(props) {
  return (
    <div className="navProfile">
      <img
        className="icon notificationBell"
        alt="bell"
        src={iconBell}>
      </img>
      <img
        className="icon profilePhoto"
        alt="pp"
        src={iconProfilePhoto}>
      </img>
    </div>
    );
}

export default NavProfile;