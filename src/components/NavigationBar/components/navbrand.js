import React, { Component, Fragment } from 'react';
import './navbrand.css'

import logoPmb from "../../../assets/pmb-logo.png";
import logoRistek from "../../../assets/ristek-logo.png";


function NavBrand(props) {
	return (
    <div className="navBrand">
      <img
        className="logo ristekLogo"
        alt="ristek"
        src={logoRistek}>
      </img>
      <img
        className="logo pmbLogo"
        alt="pmb"
        src={logoPmb}>
      </img>
    </div>
	);
}

export default NavBrand;
